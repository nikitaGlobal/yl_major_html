$(document).ready(function(){


// modal
$('.open-modal').click(function (e) {
  e.preventDefault();
  $($(this).attr('href')).addClass('active');
});

$('.close').click(function (e) {
  e.preventDefault();
  $('.modal').removeClass('active');
});

$('.modal').click(function (e) {
  if ($(e.target).hasClass('modal') && $(e.target).hasClass('active')) {
    $(e.target).removeClass('active')
  }
});

// map

  ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [54.994235, 82.700058],
            zoom: 11,
            controls: []
        }),
        

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        });

    myMap.geoObjects
        .remove('searchControl')
        .add(new ymaps.Placemark([55.007163, 82.878550], {
      }, {
          preset: 'islands#circleDotIcon',
          iconColor: 'red'
      }));
    myMap.behaviors.disable('scrollZoom');
});


$(document).scroll(function (event) {
  if ($(document).scrollTop() > 100) {
    $('header').addClass('active');
  } else {
    $('header').removeClass('active');
  };
});

});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCl7XG5cblxuLy8gbW9kYWxcbiQoJy5vcGVuLW1vZGFsJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAkKCQodGhpcykuYXR0cignaHJlZicpKS5hZGRDbGFzcygnYWN0aXZlJyk7XG59KTtcblxuJCgnLmNsb3NlJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAkKCcubW9kYWwnKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG59KTtcblxuJCgnLm1vZGFsJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgaWYgKCQoZS50YXJnZXQpLmhhc0NsYXNzKCdtb2RhbCcpICYmICQoZS50YXJnZXQpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xuICAgICQoZS50YXJnZXQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICB9XG59KTtcblxuLy8gbWFwXG5cbiAgeW1hcHMucmVhZHkoZnVuY3Rpb24gKCkge1xuICAgIHZhciBteU1hcCA9IG5ldyB5bWFwcy5NYXAoJ21hcCcsIHtcbiAgICAgICAgICAgIGNlbnRlcjogWzU0Ljk5NDIzNSwgODIuNzAwMDU4XSxcbiAgICAgICAgICAgIHpvb206IDExLFxuICAgICAgICAgICAgY29udHJvbHM6IFtdXG4gICAgICAgIH0pLFxuICAgICAgICBcblxuICAgICAgICBteVBsYWNlbWFyayA9IG5ldyB5bWFwcy5QbGFjZW1hcmsobXlNYXAuZ2V0Q2VudGVyKCksIHtcbiAgICAgICAgfSk7XG5cbiAgICBteU1hcC5nZW9PYmplY3RzXG4gICAgICAgIC5yZW1vdmUoJ3NlYXJjaENvbnRyb2wnKVxuICAgICAgICAuYWRkKG5ldyB5bWFwcy5QbGFjZW1hcmsoWzU1LjAwNzE2MywgODIuODc4NTUwXSwge1xuICAgICAgfSwge1xuICAgICAgICAgIHByZXNldDogJ2lzbGFuZHMjY2lyY2xlRG90SWNvbicsXG4gICAgICAgICAgaWNvbkNvbG9yOiAncmVkJ1xuICAgICAgfSkpO1xuICAgIG15TWFwLmJlaGF2aW9ycy5kaXNhYmxlKCdzY3JvbGxab29tJyk7XG59KTtcblxuXG4kKGRvY3VtZW50KS5zY3JvbGwoZnVuY3Rpb24gKGV2ZW50KSB7XG4gIGlmICgkKGRvY3VtZW50KS5zY3JvbGxUb3AoKSA+IDEwMCkge1xuICAgICQoJ2hlYWRlcicpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgfSBlbHNlIHtcbiAgICAkKCdoZWFkZXInKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gIH07XG59KTtcblxufSk7Il0sImZpbGUiOiJtYWluLmpzIn0=
