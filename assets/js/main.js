$(document).ready(function(){


// modal
$('.open-modal').click(function (e) {
  e.preventDefault();
  $($(this).attr('href')).addClass('active');
});

$('.close').click(function (e) {
  e.preventDefault();
  $('.modal').removeClass('active');
});

$('.modal').click(function (e) {
  if ($(e.target).hasClass('modal') && $(e.target).hasClass('active')) {
    $(e.target).removeClass('active')
  }
});

// map

  ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [54.994235, 82.700058],
            zoom: 11,
            controls: []
        }),
        

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
        });

    myMap.geoObjects
        .remove('searchControl')
        .add(new ymaps.Placemark([55.007163, 82.878550], {
      }, {
          preset: 'islands#circleDotIcon',
          iconColor: 'red'
      }));
    myMap.behaviors.disable('scrollZoom');
});


$(document).scroll(function (event) {
  if ($(document).scrollTop() > 100) {
    $('header').addClass('active');
  } else {
    $('header').removeClass('active');
  };
});

});

